var cookbooks_json = `{
    "account_ui": {
        "versions": [
            "0.26.0",
            "0.25.0",
            "0.24.0",
            "0.23.0",
            "0.22.0",
            "0.20.0",
            "0.18.0",
            "0.16.0",
            "0.14.0",
            "0.13.0",
            "0.12.0",
            "0.11.0",
            "0.10.0",
            "0.6.0",
            "0.4.0",
            "0.3.0"
        ],
        "description": {
            "current": "0.4.0",
            "versions": {
                "0.4.0": {
                    "ticket": "DEVOPS-10910",
                    "description": " Some description body:",
                    "commits": {
                        "346f5fa": {
                            "message": "Commit Body 1",
                            "author": "Vanya Programmer",
                            "mailto": "mail@domain.com"
                        }
                    }
                },
                "0.3.0": {
                    "ticket": "DEVOPS-59310",
                    "description": "Some description body:",
                    "commits": {
                        "346f5fa": {
                            "message": "Commit Body 1",
                            "author": "john doe",
                            "mailto": "mail2@domain.com"
                        },
                        "903c512": {
                            "message": "Commit Body 2",
                            "author": "mike doe",
                            "mailto": "mail@domain.com"
                        }
                    }
                }
            }
        }
    },
    "account_xapi": {
        "versions": [
            "0.14.0",
            "0.13.0",
            "0.12.0",
            "0.11.0",
            "0.10.0",
            "0.6.0",
            "0.4.0",
            "0.3.0"
        ],
        "description": {
            "current": "0.3.0",
            "versions": {
                "0.4.0": {
                    "ticket": "DEVOPS-20910",
                    "description": "Some description body:",
                    "commits": {
                        "346f5fa": {
                            "message": "Commit Body 1",
                            "author": "Vanya Programmer",
                            "mailto": "mail@domain.com"
                        }
                    }
                },
                "0.3.0": {
                    "ticket": "DEVOPS-59310",
                    "description": "Some description body:",
                    "commits": {
                        "346f5fa": {
                            "message": "Commit Body 1",
                            "author": "john doe",
                            "mailto": "mail2@domain.com"
                        },
                        "903c512": {
                            "message": "Commit Body 2",
                            "author": "mike doe",
                            "mailto": "mail@domain.com"
                        }
                    }
                }
            }
        }
    }
}
`

var json_parsed = JSON.parse(cookbooks_json)
var description_template = `{
    "ticket": "DEVOPS-59310",
    "description": "Some description body:",
    "commits": {
        "346f5fa": {
            "message": "Commit Body 1",
            "author": "john doe",
            "mailto": "mail2@domain.com"
        },
        "903c512": {
            "message": "Commit Body 2",
            "author": "mike doe",
            "mailto": "mail@domain.com"
        }
    }
}`
var description_template_obj = JSON.parse(description_template)

for (i in json_parsed) {
    cookbook_versions = json_parsed[i].versions
    for (version in json_parsed[i].versions) {
        // p(cookbook_versions)
        // json_parsed[i].description.versions[cookbook_versions[version]]
        filled_element = json_parsed[i].description.versions[cookbook_versions[version]]
        if (!json_parsed[i].description.versions[cookbook_versions[version]]) {
            filled_element = description_template_obj
        }
        // p(filled_element)
        // filled_element.ticket = description_templatedescription_template
        // p(fillrgb(81.6%, 79.9%, 79.9%)#bdb6b6ed_element.ticket)
    }
}
p(JSON.stringify(json_parsed))

function p(print) {
    console.log(print)
}