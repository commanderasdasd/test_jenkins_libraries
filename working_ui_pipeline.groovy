import com.cwctravel.hudson.plugins.extended_choice_parameter.ExtendedChoiceParameterDefinition
import test_libs.*



pipeline {
    agent any
    stages{
        stage('prepare environment') {
            steps {
            checkout([$class: 'GitSCM', branches: [[name: '*/master']],
                        userRemoteConfigs: [[url: 'git@bitbucket.org:Commander_asdasd/test_jenkins_libraries.git']]])
        }
        }
        stage('CollectingInfo') {
            steps {
             script{
                   println("Worspace is: ${WORKSPACE}")
                   sh 'python3 ${WORKSPACE}/prepare_cookbooks_UI.py'
                }
            }
        }
        stage('UserInput'){
            steps {
               script{
                    String cookbooksJson = new File("${WORKSPACE}/UI.json").text
                    def cookbooksParsed = readJSON text: cookbooksJson

                    node(){
                        def parameterList = []
                        def inputAsMap = cookbookUtils.prepareUI(cookbooksParsed)
                        def asJson = { obj ->
                            def jsonBuilder = new groovy.json.JsonBuilder(obj)
                            return jsonBuilder.toString()
                        }
                        def jsonText = asJson(inputAsMap)

                        def jsonGroovyScript = """
                            import org.boon.Boon;
                            def jsonEditorOptions = Boon.fromJson(/\${jsonText}/);
                            return jsonEditorOptions
                        """
                        String jsViewFile = new File("${WORKSPACE}/generating_description.js").text
                        def jsString = sprintf("var cookbooks_json = `%s`;%s", cookbooksParsed, jsViewFile)
                        // @NonCPS
                        def jsonParams = new ExtendedChoiceParameterDefinition(
                            'Cookbooks', //String name,
                            'PT_JSON', //String type,
                            null, //String value,
                            null, //String projectName,
                            null, //String propertyFile,
                            jsonGroovyScript, //String groovyScript,
                            null, //String groovyScriptFile,
                            "jsonText=$jsonText", //String bindings,
                            '', //String groovyClasspath,
                            null, //String propertyKey,
                            null, //String defaultValue,
                            null, //String defaultPropertyFile,
                            null, //String defaultGroovyScript,
                            null, //String defaultGroovyScriptFile,
                            null, //String defaultBindings,
                            null, //String defaultGroovyClasspath,
                            null, //String defaultPropertyKey,
                            null, //String descriptionPropertyValue,
                            null, //String descriptionPropertyFile,
                            null, //String descriptionGroovyScript,
                            null, //String descriptionGroovyScriptFile,
                            null, //String descriptionBindings,
                            null, //String descriptionGroovyClasspath,
                            null, //String descriptionPropertyKey,
                            "/var/jenkins_home/generating_description.js", //String javascriptFile,
                            jsString, //String javascript,
                            false, //boolean saveJSONParameterToFile,
                            false, //boolean quoteValue,
                            10, //int visibleItemCount,
                            '', //String description,
                            ',' //String multiSelectDelimiter
                        )
                        parameterList << jsonParams
                        def form = input(
                            id: 'form', message: 'input parameters', parameters: parameterList
                        )
                        env.FORM = form
                    }
            }
        }
        }
        stage('DoingStuff'){
            environment {
                def envFormParsed = readJSON text: env.FORM
                def outputCookboks = cookbookUtils.separateChanged(envFormParsed)
            }
            steps {
                    echo "Choosen: ${env.FORM}"
                    echo "changed: ${outputCookboks[0]}, not changed: ${outputCookboks[1]} "
            }
        }
        stage('Cleanup'){
            steps {
                fileDeleteOperation(includes:"${WORKSPACE}/UI.json", excludes:'')
            }
        }
    }
}