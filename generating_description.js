var cookbooks_json = '{"un_nodejs":{"description":{"current":"None","versions":{"1.0.8":{"description":"TODO version description","ticket":"TODO Jira ticket link","commits":{"f801486e":{"message":"B-79726 update java cookbook dependency to latest version from chef supermarket","mailto":"joseph.browning@macys.com","author":"Joseph Browning"},"3bcee73c":{"message":"WDSDO-25375 [ci-skip] Increase version up to v1.0.8","mailto":"jenkinswds.re@macys.com","author":"jenkins"},"cddd7ced":{"message":"Merge branch java_dependency_cookboook_version into masterB-79726 update java cookbook dependency to latest version from chef supermarketSee merge request !7","mailto":"b005044@federated.fds","author":"Amandeep Singh"}}},"1.0.9":{"description":"TODO version description","ticket":"TODO Jira ticket link","commits":{"51a4baa7":{"message":"WDSDO-25375 [ci-skip] Increase version up to v1.0.9","mailto":"jenkinswds.re@macys.com","author":"jenkins"},"efb40f24":{"message":"Merge branch java_dependency_cookboook_version into masterB-79726 reverting java dependency to 1.40.0See merge request !8","mailto":"joseph.browning@macys.com","author":"Joseph Browning"},"45ac3ed1":{"message":"B-79726 reverting java dependency to 1.40.0","mailto":"joseph.browning@macys.com","author":"Joseph Browning"}}},"1.0.12":{"description":"TODO version description","ticket":"TODO Jira ticket link","commits":{"d2e1d570":{"message":"Merge branch DEVOPS-343 into masterDEVOPS-343 added using new function from kitchen.lib: imageName# Please allow 2 guys to review your change (one of these guys should be a core approver)See merge request !11","mailto":"timofey.zaykin@macys.com","author":"Timofey Zaykin"},"9e5a2d0e":{"message":"DEVOPS-343 added using new function from kitchen.lib: imageName","mailto":"eugene.senchenko@macys.com","author":"Eugene Senchenko"},"e2c03edf":{"message":"WDSDO-25375 [ci-skip] Increase version up to v1.0.12","mailto":"jenkinswds.re@macys.com","author":"jenkins"}}},"1.1.0":{"description":"TODO version description","ticket":"TODO Jira ticket link","commits":{"59a7f8ec":{"message":"Merge branch CD-87 into masterCD-87 Fix user home folder creationSee merge request !13","mailto":"evgeny.volkov@macys.com","author":"Evgeny Volkov"},"5c9ef6c2":{"message":"CD-87 Fix user home folder creation","mailto":"evgeny.volkov@macys.com","author":"Evgeny Volkov"},"535ba7ea":{"message":"CD-484 update .gitignore, chefignore","mailto":"evgeny.volkov@macys.com","author":"Evgeny Volkov"},"729acce2":{"message":"CD-87 remove test.sh","mailto":"evgeny.volkov@macys.com","author":"Evgeny Volkov"},"68f3bc0a":{"message":"Merge branch CD-484 into masterCD-484 update .gitignore, chefignoreSee merge request !14","mailto":"evgeny.volkov@macys.com","author":"Evgeny Volkov"},"1a69b731":{"message":"DEVOPS-746 [ci-skip] Increase version up to v1.1.0","mailto":"jenkinswds.re@macys.com","author":"jenkins"}}},"1.0.6":{"description":"TODO version description","ticket":"TODO Jira ticket link","commits":{"487bddee":{"message":"WDSDO-26012 Added real tests for un_nodejs","mailto":"sergey.parshev@macys.com","author":"Sergey Parshev"},"b362c801":{"message":"[ci-skip] Increase version up to v1.0.6","mailto":"jenkinswds.re@macys.com","author":"jenkins"},"cf2b8d5b":{"message":"Merge branch WDSDO-26012 into masterWDSDO-26012 Added real tests for un_nodejsSee merge request !6","mailto":"john.ham@macys.com","author":"John Ham"}}},"1.0.10":{"description":"TODO version description","ticket":"TODO Jira ticket link","commits":{"2cc2d75a":{"message":"WDSDO-25375 [ci-skip] Increase version up to v1.0.10","mailto":"jenkinswds.re@macys.com","author":"jenkins"},"3ce81a57":{"message":"Merge branch java_dependency_cookboook_version into masterB-79726 update java cookbook dependency to latest version from chef supermarketSee merge request !9","mailto":"b005044@federated.fds","author":"Amandeep Singh"},"51ac0a79":{"message":"B-79726 update java cookbook dependency to latest version from chef supermarket","mailto":"joseph.browning@macys.com","author":"Joseph Browning"}}},"1.2.0":{"description":"TODO version description","ticket":"TODO Jira ticket link","commits":{"912a20f6":{"message":"B-79639 Add the backup recipe for nodejs cookbook","mailto":"jiaxiang.zhu@macys.com","author":"Peter Zhu"},"500029aa":{"message":"Merge branch b004660_int_master_nodejs_backup into masterB-79639 Add the backup recipe for nodejs cookbookSee merge request !15","mailto":"b005044@federated.fds","author":"Amandeep Singh"},"74a67ea6":{"message":"DEVOPS-746 [ci-skip] Increase version up to v1.2.0","mailto":"jenkinswds.re@macys.com","author":"jenkins"}}},"1.0.11":{"description":"TODO version description","ticket":"TODO Jira ticket link","commits":{"031c84e7":{"message":"WDSDO-25375 [ci-skip] Increase version up to v1.0.11","mailto":"jenkinswds.re@macys.com","author":"jenkins"},"52914a6a":{"message":"B-79726 moving runtime dependency from metadata to berksfile","mailto":"dmitry.kulishenko@macys.com","author":"Dmitry Kulishenko"},"2355e61d":{"message":"Merge branch B-79726_dep_fix into masterB-79726 moving runtime dependency from metadata to berksfileSee merge request !10","mailto":"dmitry.kulishenko@macys.com","author":"Dmytro Kulishenko"}}}}},"versions":["1.1.0","1.2.0","1.0.8","1.0.11","1.0.9","1.0.12","1.0.6","1.0.10"]}}'
var style = document.createElement('style');
style.type = 'text/css';
style.innerHTML = `
.commits { background-color: #c2bebe; margin: 1em }
.cookbook_name { clear:both; }
.commit_holder { padding: 1em }
.description_holder { background-color: #ece6e6}
.cookbook_name { font-size: x-large; }
.curr_version { font-size: large; }
.version_holder { padding: 0.5em; }
`;


document.getElementsByTagName('head')[0].appendChild(style);

console.log(`Will parse ${cookbooks_json}`)
var json_parsed = JSON.parse(cookbooks_json)

function take_versions(structure) {
    for (version in structure) {
        return version
    }
}

function take_name(cookbook) {
    Object.keys(cookbook)[0];
}


function iterate_commits(element_data, element_append_to) {
    /* Create commits structure under the element_append_to*/

    commits = element_data.commits
    for (commit in commits) {
        var commit_holder = document.createElement('div')
        var commit_id = document.createElement('div')
        var commit_msg = document.createElement('div')
        var commit_author = document.createElement('div')

        commit_holder.className = "commit_holder"
        commit_id.className = "commit_id"
        commit_id.setAttribute("commit_id", commit)
        commit_msg.className = "commit_msg"
        commit_author.className = "commit_author"

        commit_id.innerHTML = commit + "<br>"
        commit_msg.textContent = commits[commit].message
        commit_author.innerHTML = "Author: " + commits[commit].author + "<br>"

        commit_holder.appendChild(commit_id)
        commit_holder.appendChild(commit_msg)
        commit_holder.appendChild(commit_author)
        element_append_to.appendChild(commit_holder)

    }

}

function iterate_versions(element_data, element_append_to) {
    /* create versions structure under element_append_to */

    var versions = element_data.description.versions
    for (version in versions) {
        var version_holder = document.createElement('div')
        var version_label = document.createElement('div')
        var ticket = document.createElement('div')
        var ticket_description = document.createElement('div')
        var commits = document.createElement('div')


        version_holder.className = "version_holder"
        version_holder.setAttribute("version", version)
        version_label.className = "version_label"
        ticket.className = "ticket"
        ticket_description.className = "ticket_description"
        commits.className = "commits"

        version_label.textContent = version
        ticket.textContent = versions[version].ticket
        ticket_description.textContent = versions[version].version_description
        iterate_commits(versions[version], commits)

        element_append_to.appendChild(version_holder)
        version_holder.appendChild(version_label)
        version_holder.appendChild(ticket)
        version_holder.appendChild(ticket_description)
        version_holder.appendChild(commits)
    }
    return version_label
}

function generate_element(name) {

    found_element = json_parsed[name];
    if (found_element) {
        var description_holder = document.createElement('div');
        var source_url = document.createElement('a');
        var cookbook_name = document.createElement('span');
        var curr_version_elem = document.createElement('span');
        var versions = document.createElement('div');

        description_holder.className = "description_holder";
        cookbook_name.className = "cookbook_name";
        description_holder.setAttribute("cookbook", name)
        curr_version_elem.className = "curr_version";
        versions.className = "versions";

        source_url.href = found_element.source_url
        cookbook_name.innerHTML = name + "</br>"
        curr_version_elem.textContent = "Current version: " + found_element.description.current;

        description_holder.appendChild(source_url);
        source_url.appendChild(cookbook_name)
        description_holder.appendChild(curr_version_elem);
        description_holder.appendChild(versions);

        var version_elements = []
        version_elements = iterate_versions(found_element, versions);
    } else {
        var description_holder = document.createElement('div');
        description_holder.className = "description_holder";
        description_holder.textContent = "NO DATA FOUND"
    }
    return description_holder;
}

function enumerate_sel_options(selector) {

    /* enumerating and set display = none by default */

    var element_name = selector.name.split("[")[1].split("]")[0]
    var i = 0;
    for (option in selector.children) {
        try {
            selector.children[option].setAttribute("count", i);
            var version = selector.children[option]["value"]
            document.querySelectorAll(`[cookbook="${element_name}"]`)[0].querySelector(`div[version="${version}"`).setAttribute("count", i);
            document.querySelectorAll(`[cookbook="${element_name}"]`)[0].querySelector(`div[version="${version}"`).style.display = "none"
        } catch (err) {
            console.log("Can't enumerate", err)
        }
        i++
    }
}


function place_structure() {
    /* Entry point. placing description and callback on page after all is done */

    provide_callback_to_page(selector_callback);
    var selectors = document.getElementsByTagName('select');
    for (select in selectors) {
        document.getElementsByTagName('select')[select].setAttribute("onchange", "selector_callback(this.value, this.name.split(\"[\")[1].split(\"]\")[0], this)");
        try {
            var element_name = selectors[select].name.split("[")[1].split("]")[0]
            var append_element = selectors[select].parentElement.parentElement.parentElement
            selectors[select].parentElement.style = "width: 20%; float:left";
            var placed_element = generate_element(element_name)
            placed_element.style = "width: 80%; float:right";
            append_element.appendChild(placed_element)
            var clear_element = document.createElement('div');
            clear_element.style = "clear: both"
            append_element.appendChild(clear_element);

            enumerate_sel_options(selectors[select]);
            sortOptions(selectors[select]);
        } catch (err) {
            console.log("no element")
        }
    }
}

function selector_callback(version, cookbook_name, selector_obj) {
    /* collapsing elements until higher than choosen option */
    /* counts_all is map that stores enumerated version list for every cookbook  */

    var counts_all = {}
    var options = selector_obj.children

    function show_count(count_passed) {
        try {
            document.querySelectorAll(`[cookbook="${cookbook_name}"]`)[0].querySelector(`div[count="${count_passed}"]`).style.display = "block"
        } catch (err) {
            console.log("error cath: ", err)
        }
    }

    function hide_count(count_passed) {
        try {
            document.querySelectorAll(`[cookbook="${cookbook_name}"]`)[0].querySelector(`div[count="${count_passed}"]`).style.display = "none"
        } catch (err) {
            console.log("error cath: ", err)
        }
    }

    function count_options() {
        counts_all[cookbook_name] = []
        for (option in options) {
            try {
                counts_all[cookbook_name].push(options[option].getAttribute("count"));
            } catch (err) {
                console.log("error catch", err)
            }
        }
        console.log("Elements counted", counts_all[cookbook_name])
    }

    function define_state(count_passed, count_all) {
        /* count_to_ variables storing   */
        var count_to_show = []
        var count_to_hide = []
        console.log(count_passed, count_all)
        if (parseInt(count_passed) === 0) {
            for (i in count_all) {
                count_to_hide.push(i)
                console.log("None selected")
            }
        } else {
            for (i in count_all) {
                if (count_all.hasOwnProperty(i)) {
                    if (i >= parseInt(count_passed)) {
                        count_to_show.push(i)
                    } else {
                        count_to_hide.push(i)
                    }
                }
            }
        }
        count_to_show.map(show_count)
        count_to_hide.map(hide_count)
        console.log("Show: ", count_to_show, "hide: ", count_to_hide)
    }
    try {
        var count = selector_obj.querySelectorAll(`[value="${version}"]`)[0].getAttribute("count")
    } catch (err) {
        console.log("error catch: ", err)
    }
    console.log("count", count)
    count_options()
    define_state(count, counts_all[cookbook_name])

}

function provide_callback_to_page(function_provided) {
    var new_script = document.createElement("script")
    new_script.innerHTML = function_provided
    document.getElementsByTagName("head")[0].appendChild(new_script)
}

function compareVersions(a, b) {
    var result = a.split(".")[0] - b.split(".")[0];
    if (result === 0) {
        result = a.split(".")[1] - b.split(".")[1];
        if (result === 0) {
            result = a.split(".")[2] - b.split(".")[2];
        }
    }
    return result
}

function sortOptions(selElem) {
    var tmpAry = new Array();
    console.log(selElem);
    try {
        for (var i=0;i<selElem.options.length;i++) {
            tmpAry[i] = new Array();
            tmpAry[i][0] = selElem.options[i].text;
            tmpAry[i][1] = selElem.options[i].getAttribute("count");
        }
        tmpAry.sort();
        while (selElem.options.length > 0) {
            selElem.options[0] = null;
        }
        for (var i=0;i<tmpAry.length;i++) {
            var op = new Option(tmpAry[i][0]);
            op.setAttribute("count",tmpAry[i][1])
            selElem.options[i] = op;
        }
    }
    
    return;
}

function iterateSelectorsToSort() {
    var selectors = document.getElementsByTagName('select');
    for (select in selectors) {
        sortOptions(document.getElementsByTagName('select')[select]);
        // console.log(document.getElementsByTagName('select')[select]);
    }
}

place_structure();
// iterateSelectorsToSort();