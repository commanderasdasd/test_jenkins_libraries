"""
ORIGINAL SCRIPT EXAMPLE
Script to prepare announcement message
Environment vars:
 - SERVER  - server to deploy the new artifact
 - GIT_TAG - current git tag from repository
 - PREVIOUS_GIT_TAG - previous git tag of the artifact that was deployed to production
 - ANNOUNCE_COMMENT - additional information about the deployment (you can use html tags)
 - REPO_NAME - repository name
 - WAIT_TIME - minutes to wait before continue
 - ARTIFACT_URL - cookbooks artifact URL
"""

import os
import re
import subprocess
import datetime
import logging
import shutil
import json

from distutils.version import LooseVersion

logging.basicConfig(level=logging.DEBUG)
LOG = logging.getLogger(__name__)


def get_repo_url(repo_name):
    return "http://code.devops.fds.com/devops/{repo_name}".format(repo_name=repo_name)


def get_git_log(tag_from, tag_to, repo_name):
    if tag_from:
        git_command = "git log --no-merges --date=local " \
                      "--pretty=format:------%ncommit:%H%nauthor:%an%nemail:%ae%ndate:%ad%nsubject:%s {}...{}".format(
                                                                                                            tag_from,
                                                                                                            tag_to)
    else:
        git_command = "git log --no-merges --date=local " \
                      "--pretty=format:------%ncommit:%H%nauthor:%an%nemail:%ae%ndate:%ad%nsubject:%s {}".format(tag_to)

    path_to_repo = os.path.join(os.environ["WORKSPACE"], repo_name)
    try:
        return subprocess.check_output(git_command, shell=True, cwd=path_to_repo)
    except subprocess.CalledProcessError as ex:
        LOG.error("Unable to get git log between {tag_from} and {tag_to} tags: {error}".format(tag_from=tag_from,
                                                                                               tag_to=tag_to,
                                                                                               error=ex.output))


def parse_git_log(git_log):
    out = {}
    for line in git_log.splitlines():
        if line == '------':
            commit = {}
        else:
            line = line.split(':', 1)
            if len(line) > 1:
                commit[line[0]] = line[1]
                if line[0] == 'subject':
                    try:
                        ticket = re.search('^[A-Z]+-[0-9]+', line[1]).group()
                    except AttributeError:
                        # The case for commit "Revert "Merge branch '[ticket]' into master""
                        try:
                            ticket = re.search('[A-Z]+-[0-9]+', line[1]).group()
                            commit[line[0]] = ticket + ' ' + line[1]
                        except AttributeError:
                            ticket = "NONE"
                            commit[line[0]] = ticket + ' ' + line[1]

                    if ticket in out:
                        out[ticket].append(commit)
                        out[ticket] = sorted(out[ticket], key=lambda x: datetime.datetime.strptime(
                            x['date'], '%a %b %d %H:%M:%S %Y'))
                    else:
                        out[ticket] = [commit]
    return out


def get_deployed_cookbooks(server):
    knife_command = "knife cookbook list --server-url '{server}' " \
                    "--key '{workspace}/hudson/cloudpants/keys/chef.devops.fds.com_client.pem' " \
                    "--config '{workspace}/hudson/cloudpants/knife.rb'".format(server=server,
                                                                               workspace=os.environ['WORKSPACE'])

    try:
        output = subprocess.check_output(knife_command, shell=True, cwd=os.environ['WORKSPACE'])

        return {x.split()[0]: x.split()[1] for x in output.splitlines()}
    except subprocess.CalledProcessError as ex:
        LOG.error("Unable to get deployed cookbooks from {server}: {error}".format(server=server,
                                                                                   error=ex.output))


def get_cookbooks_artifact(version, dir):
    artifact_path = os.path.join(os.environ["WORKSPACE"], "cookbooks-{version}.tar.gz".format(version=version))
    download_cmd = "wget -NqO {artifact_path} '{url}'".format(artifact_path=artifact_path,
                                                                 url=os.environ['ARTIFACT_URL'])
    extract_cmd = "tar xzfm {artifact_path}".format(artifact_path=artifact_path)
    target_path = os.path.join(os.environ["WORKSPACE"], dir)

    LOG.info("Downloading cookbook artifact ({version})".format(type=dir, version=version))
    subprocess.check_call(download_cmd, shell=True, cwd=os.environ["WORKSPACE"])

    try:
        os.mkdir(target_path)
    except Exception as e:
        print("Can't make dir, err: ", e)

    LOG.info("Extracting cookbook artifact ({version})".format(type=dir, version=version))
    subprocess.check_call(extract_cmd, shell=True, cwd=target_path)
    os.remove(artifact_path)

    return target_path


def generate_cookbook_changelog(current_tag):
    current_artifact = get_cookbooks_artifact(current_tag, "current")
    current_cookbooks = os.path.join(current_artifact, "cookbooks")

    previous_cookbooks = get_deployed_cookbooks(os.environ['SERVER'])

    version_map = {}

    for cookbook in os.listdir(current_cookbooks):
        current_cookbook = os.path.join(current_cookbooks, cookbook, "metadata.json")

        version_map[cookbook] = {}
        source_url = ""

        if cookbook in previous_cookbooks.keys():
            version_map[cookbook]['previous_tag'] = previous_cookbooks[cookbook]
        else:
            version_map[cookbook]['previous_tag'] = None

        if os.path.exists(current_cookbook) and os.path.isfile(current_cookbook):
            with open(current_cookbook, 'r') as metadata_file:
                metadata = json.load(metadata_file)
                version_map[cookbook]['current_tag'] = metadata['version']

                if 'source_url' in metadata.keys():
                    source_url = metadata['source_url']

        version_map[cookbook]['source_url'] = source_url if source_url.endswith('/') else source_url + "/"

    git_dir = os.path.join(os.environ["WORKSPACE"], "git")
    if not os.path.exists(git_dir):
        os.mkdir(git_dir)

    changelog = {}
    for cookbook, data in version_map.items():
        if 'current_tag' not in data.keys() and 'previous_tag' in data.keys():
            LOG.info("Detected cookbook deletion: {cookbook} ({version})".format(cookbook=cookbook,
                                                                                 version=data['previous_tag']))
            continue

        if data['previous_tag'] != data['current_tag'] and 'code.devops.fds.com' in data['source_url']:
            LOG.info("Detected change in {cookbook}: ({previous} -> {current})".format(cookbook=cookbook,
                                                                                       previous=data['previous_tag'],
                                                                                       current=data['current_tag']))
            changelog[cookbook] = {
                'changelog': {},
                'repo_url': data['source_url'],
                'previous_tag': data['previous_tag'],
                'current_tag': data['current_tag']
            }

            git_clone_cmd = "git clone git@code.devops.fds.com:chef/{cookbook}.git 2> /dev/null".format(
                                                                 cookbook=cookbook)
            try:
                subprocess.check_call(git_clone_cmd, shell=True, cwd=git_dir)
            except Exception:
                pass

            if data['previous_tag']:
                git_tags_cmd = "git tag | sort -V | sed '/v{previous}/,/v{current}/!d'".format(
                                                                                        previous=data['previous_tag'],
                                                                                        current=data['current_tag'])
            else:
                git_tags_cmd = "git tag | sort -V | sed '1,/v{current}/!d'".format(current=data['current_tag'])

            cookbook_dir = os.path.join(git_dir, cookbook)

            git_tags = subprocess.check_output(git_tags_cmd, shell=True, cwd=cookbook_dir).splitlines()
            git_tag_groups = []

            if not data['previous_tag']:
                git_tags = [None] + git_tags

            for i in range(len(git_tags) - 1):
                git_tag_groups += [(git_tags[i], git_tags[i + 1])]

            for previous_tag, current_tag in git_tag_groups:
                changelog[cookbook]['changelog'][current_tag] = parse_git_log(get_git_log(previous_tag, current_tag,
                                                                              os.path.join("git", cookbook)))

            shutil.rmtree(cookbook_dir)

    shutil.rmtree(current_artifact)

    return changelog


def generate_changelog_message(changelog):
    msg = ""

    for cookbook, changelog in sorted(changelog.items(), key=lambda x: x[0]):
        msg += "<div style='background-color:#eee; padding:10px; margin:0.5em'><p>" \
               "<b><a href={repo_url}>{cookbook}</a>:</b> " \
               "<a href={repo_url}compare/v{previous_tag}...v{current_tag} style='font:1.4em monospace'>" \
               "{previous_tag} - {current_tag}</a></p>".format(cookbook=cookbook,
                                                               repo_url=changelog['repo_url'],
                                                               previous_tag=changelog['previous_tag'],
                                                               current_tag=changelog['current_tag'])

        for version, tickets in sorted(changelog['changelog'].items(), key=lambda x: LooseVersion(x[0])):
            msg += "<div style='background-color:#ddd; padding:10px; margin:0.5em'>" \
                   "<p><a href={repo_url}tags/{version} style='font:1.4em monospace'>{version}</a></p>".format(
                                                                                        repo_url=changelog['repo_url'],
                                                                                        version=version)

            for ticket, data in sorted(tickets.items(), key=lambda x: datetime.datetime.strptime(
                    x[1][-1]['date'], '%a %b %d %H:%M:%S %Y')):
                if ticket in ['DEVOPS-746']:
                    continue

                msg += "<div style='padding:10px; margin:0.5em'><p>{ticket}:</p>".format(
                                                                                    ticket=generate_ticket_link(ticket))
                msg += "<ul>"
                for content in data:
                    subject = content['subject'][len(ticket):] if ticket else content['subject']
                    msg += "<li><a href='{repo_url}commit/{commit}' style='font:1.4em monospace'>{commit:.7}</a>" \
                           "<span>{subject}</span> (<a href='mailto:{email}'>{author}</a>)</li>".format(
                                                                                        repo_url=changelog['repo_url'],
                                                                                        commit=content['commit'],
                                                                                        subject=subject,
                                                                                        email=content['email'],
                                                                                        author=content['author'])
                msg += "</ul>"
                msg += "</div>"
            msg += "</div>"
        msg += "</div>"

    return msg


def generate_ticket_link(ticket):
    if re.match('^([BD]-[0-9]+)$', ticket):
        v_one_cmd = "curl -sSL -k -H 'Authorization: Bearer {v1_token}' -X POST -H 'Content-Type: application/json' " \
                    "-d '{{\"from\":\"Story\",\"where\":{{\"Number\":\"{ticket}\"}},\"select\":[\"Name\",\"ID\"]}}' " \
                    "'https://www14.v1host.com/Macyscom/query.v1'".format(v1_token=os.environ['V1_TOKEN'],
                                                                          ticket=ticket)

        v_one_issue = json.loads(subprocess.check_output(v_one_cmd, shell=True, cwd=os.environ['WORKSPACE']))

        try:
            return "<span style='font:1.4em monospace'>" \
                   "<a href='https://www14.v1host.com/Macyscom/story.mvc/Summary?oidToken={oid}'>{ticket}</a> " \
                   "{summary}</span>".format(ticket=ticket, oid=v_one_issue[0][0]["ID"]["_oid"],
                                             summary=v_one_issue[0][0]["Name"])
        except KeyError:
            return "<span style='font:1.4em monospace'>{ticket}</span>".format(ticket=ticket)
    else:
        jira_cmd = "curl -sSL -k -u '{jira_user}:{jira_password}' " \
                   "'https://jira.federated.fds/rest/api/latest/issue/{ticket}'".format(
                                                                            jira_user=os.environ['JIRA_USER'],
                                                                            jira_password=os.environ['JIRA_PASSWORD'],
                                                                            ticket=ticket)
        jira_issue = json.loads(subprocess.check_output(jira_cmd, shell=True, cwd=os.environ['WORKSPACE']))

        try:
            return "<span style='font:1.4em monospace'><a href='https://jira.federated.fds/browse/{ticket}'>" \
                   "{ticket}</a> {summary}</span>".format(ticket=ticket, summary=jira_issue['fields']['summary'])
        except KeyError:
            return "<span style='font:1.4em monospace'><a href='https://jira.federated.fds/browse/{ticket}'>" \
                   "{ticket}</a></span>".format(ticket=ticket)


def generate_message(previous_tag, current_tag, instance, time_to_wait, repo_name, build_url, build_number):
    # Get deploy time start to inform in the email message
    deploy_time_start = (datetime.datetime.now() +
                         datetime.timedelta(minutes=int(time_to_wait))).strftime("%Y-%m-%d %H:%M:%S")

    # Let's form the email body
    msg = "<html><body>"
    msg += "<p><a href='{BUILD_URL}'>Deploy #{BUILD_NUMBER} to <b>{SERVER}</b></a> " \
           "will be processed at {deploy_start}</p>".format(BUILD_URL=build_url, BUILD_NUMBER=build_number,
                                                            SERVER=instance, deploy_start=deploy_time_start)

    if "ANNOUNCE_COMMENT" in os.environ:
        msg += os.environ["ANNOUNCE_COMMENT"]

    parsed_git_log = parse_git_log(get_git_log(previous_tag, current_tag, repo_name))
    msg += "<div style='background-color:#eee; padding:10px; margin:0.5em'><p><b>Overall changes:</b> " \
           "<a href={repo_url}/compare/{previous_tag}...{current_tag} style='font:1.4em monospace'>" \
           "{short_previous_tag} - {short_current_tag}</a></p>".format(repo_url=get_repo_url(repo_name),
                                                                       previous_tag=previous_tag,
                                                                       current_tag=current_tag,
                                                                       short_previous_tag=previous_tag[-8:],
                                                                       short_current_tag=current_tag[-8:])
    for ticket, data in sorted(parsed_git_log.items(), key=lambda x: datetime.datetime.strptime(
                                                                            x[1][-1]['date'], '%a %b %d %H:%M:%S %Y')):
        msg += "<div style='background-color:#eee; padding:10px; margin:0.5em'><p>{ticket}:</p>".format(
                                                                                    ticket=generate_ticket_link(ticket))
        msg += "<ul>"
        for content in data:
            subject = content['subject'][len(ticket):] if ticket else content['subject']
            msg += "<li><a href='{repo_url}/commit/{commit}' style='font:1.4em monospace'>{commit:.7}</a>" \
                   "<span>{subject}</span> (<a href='mailto:{email}'>{author}</a>)</li>".format(
                                                                                    repo_url=get_repo_url(repo_name),
                                                                                    commit=content['commit'],
                                                                                    subject=subject,
                                                                                    email=content['email'],
                                                                                    author=content['author'])
        msg += "</ul>"
        msg += "</div>"
    msg += "</div>"

    if repo_name == "chef":
        msg += generate_changelog_message(generate_cookbook_changelog(current_tag))

    msg += "</body></html>"
    return msg


if __name__ == '__main__':
    try:
        previous_git_tag = os.environ["PREVIOUS_GIT_TAG"]
        git_tag = os.environ["GIT_TAG"]
        server = os.environ["SERVER"]
        wait_time = os.environ["WAIT_TIME"]
        repository_name = os.environ["REPO_NAME"]
        deploy_build_url = os.environ["DEPLOY_BUILD_URL"]
        deploy_build_number = os.environ["DEPLOY_BUILD_NUMBER"]
        LOG.info("Generating the list of changes between the last deployed artifact ({previous_git_tag}) and artifact "
                 "({git_tag}) that will be deployed to {server} in {wait_time} minutes".format(
                                                                                    previous_git_tag=previous_git_tag,
                                                                                    git_tag=git_tag,
                                                                                    server=server,
                                                                                    wait_time=wait_time))
        message = generate_message(previous_git_tag, git_tag, server, wait_time, repository_name, deploy_build_url,
                                   deploy_build_number)

        env_var = "EMAIL_MESSAGE={message}\n".format(message=message)
        print(env_var)
        env_file = os.path.join(os.environ["WORKSPACE"], "env.properties")
        f = open(env_file, 'a')
        f.write(env_var)
        f.close()
    except KeyError, e:
        LOG.error("Variable %s hasn't been passed" % str(e))