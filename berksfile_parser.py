import subprocess, os, shutil


def dev_fixtures():
    os.environ['WORKSPACE'] = "./"
    os.environ['SERVER'] = "https://chef-ha.devops.fds.com/organizations/zeus"

def parse_berksfile(berksfile_text):
    for string in berksfile_text.splitlines():
        if string[0:8] == "cookbook":
            print(string.split(" ")[1].replace('\'', "").rstrip(","))


def get_berksfile(repo="git@code.devops.fds.com:devops/chef"):
    
    command_git_clone = "git clone {}".format(repo)
    repo_name = repo.split('/')[-1]
    repo_path = os.path.join(os.environ['WORKSPACE'], repo_name)
    berksfile_path = os.path.join(repo_path, "Berksfile")

    if os.path.isdir(repo_path):
        shutil.rmtree(repo_path)
    elif os.path.isfile(repo_path):
        os.remove(repo_path)
    try:
        subprocess.check_output(command_git_clone, shell=True, cwd=os.environ['WORKSPACE'])
    except Exception as e:
            print(e)

    with open(berksfile_path, "r") as berksfile:
        berksfile_content = berksfile.read()

    return berksfile_content

if __name__ == "__main__":
    dev_fixtures()
    berksfile_content = get_berksfile()
    berksfile_cookbooks = parse_berksfile(berksfile_content)
