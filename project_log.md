# javascript

https://stackoverflow.com/questions/22767609/add-event-listener-to-dom-elements-based-on-class
addEventListener instead of .onchange

document.getElementsByClassName('control-group')[0].parentNode

## Create a straightforward implementation

Text-only

# Checkboxes
"input" step pass user input into environment variables
## JsonEditor
ExtendedChoiceParameterDefinition uses JsonEditor library syntax
### Complex logic UI
Try ty using template engine (ejs, because moustache and handlebars have no logic) - was a dead end, ejs needs to be loaded into page first). Answer is Javascript parameter in ExtendedChoiceParameterDefinition (see below)
### Collapsing drop-down lists
Strange, but drop-downs collapsing automatically (appears in "single" mode) only when "default value" is set.

## ExtendedChoiceParameterDefinition javascript string

## Using null in versions list builder causes stack overflow error

## Auto-refreshing
Disable annoying auto-refresh can be achieved via Jenkins in upper-right corner of input page

## UI elements
need to
import com.cwctravel.hudson.plugins.extended_choice_parameter.ExtendedChoiceParameterDefinition
example in vars/choices_list_template.groovy
## Scripts semantic
Pipeline code from vars/choices_list_template.groovy can be used soleley or in next form:
```
pipeline {
    steps {
        script {
            //CODE
        }
    }
}
```
## Print environment, work with environment
```
    sh 'printenv' # get all environment variables (it same as system)
```

Responce from input in script block is just a variable 

```
    def form = input(...)
    print(form)
```

Setting environment variable from script is like:

```
    env.FORM = form
```
## COnverting ENV string to list
https://stackoverflow.com/questions/15125003/convert-string-variable-to-a-list-groovy

## Parse libraries

## JSON slurper error
JsonSlurperClassic - not working
Both parse and restore functions placed in vars/cookbookUtils.groovy because of JsonSlurper bug

#TODO
* Try set env var with data from input [X]
* convert string to groovy list [x]
* Iterate over environment variables (how to get env.COOKBOOK_#{n} ?) [n]
* Add null parameter to every choice [x] (not needed)
* Collapse drop-downs [x]
* Place elements from array via template engine [n] 
* Make conditional behaviour in rendering elements via template engine [n]
* Develop json schema with description that suits my needs []
* Find how to pass that json to javascript (string substitution?) []