import requests, json, gitlab, grequests


class async_cookbooks_request():

    def __init__(self):
        self.__cookbooks_links = []

    def push_link(self, link):
        self.__cookbooks_links.append(link)

    def exception(self, request, exception):
        print("Problem: {}: {}".format(request.url, exception))

    def async_call(self, custom_headers={}):
        results = grequests.map(
            (grequests.get(u, headers=custom_headers) for u in self.__cookbooks_links),
            exception_handler=self.exception,
            size=50)
        return results


def cookbook_get_tags(cookbook=""):
    
    responce = json.loads(
            requests.get("https://code.devops.fds.com/api/v4/projects/chef%2F{cookbook}/repository/tags".format(cookbook=cookbook), 
            headers={"Private-Token": "WD2aTjFrmHqyrTjGG-Ap"}).content)
    # tags_and_dates = {}
    # print(responce)
    tags = []
    for key in responce:
        tags.append(key["name"])
        # tags_and_dates[name] = {}
    #     tags_and_dates[name]["date"] = key["commit"]["created_at"]
    #     tags_and_dates[name]["commit"] = key["commit"]["short_id"]

    return tags

def take_commits_betw_two_dates(cookbook, date1, date2):

    # print("Asking commits between dates {} and {}".format(date2, date1))
    responce_first = json.loads(requests.get("https://code.devops.fds.com/api/v4/projects/chef%2F{cookbook}/repository/commits?until={date1}".format(cookbook=cookbook, date1=date1), 
        headers={"Private-Token": "WD2aTjFrmHqyrTjGG-Ap"}).content)
    responce_second = json.loads(requests.get("https://code.devops.fds.com/api/v4/projects/chef%2F{cookbook}/repository/commits?until={date2}".format(cookbook=cookbook, date2=date2), 
        headers={"Private-Token": "WD2aTjFrmHqyrTjGG-Ap"}).content)

    commits_first = {}
    commits_second = {}

    for commit in responce_first:
        commit_id = commit["short_id"]
        commit_date = commit["authored_date"]
        commit_message = commit["message"]
        commits_first[commit_id] = {}
        commits_first[commit_id]["date"] = commit_date
        commits_first[commit_id]["message"] = commit_message

    for commit in responce_second:
        commit_id = commit["short_id"]
        commit_date = commit["authored_date"]
        commit_message = commit["message"]
        commits_second[commit_id] = {}
        commits_second[commit_id]["date"] = commit_date
        commits_second[commit_id]["message"] = commit_message
    
    commits_diff = []

    # print("commits in first set: {}".format(commits_first.keys()))
    # print("commits in second set: {}".format(commits_second.keys()))

    for commit, data in commits_second.items():
        if commit not in commits_first.keys():
            print("commit {} not in {}".format(commit, commits_first.keys()))
            commits_diff.append({commit : data})

    return commits_diff
    
def compose_diff_for_every_tag(cookbook, tags):
    
    tags_and_diffs = {}
    # dates = list(tags_and_dates.values())
    # tags = list(tags_and_dates.keys())
    for counter, tag in enumerate(tags):
        if counter > 0:
            print("Comparing {} and {}".format(tags[counter], tags[counter-1]))
            tags_and_diffs[tags[counter-1]] = commits_compare(cookbook, tags[counter], tags[counter-1])["commits"]

    return tags_and_diffs


def commits_compare(cookbook, tag_from, tag_to):
    responce = json.loads(
        requests.get("https://code.devops.fds.com/api/v4/projects/chef%2F{cookbook}/repository/compare?from={tag_from}&to={tag_to}".format(cookbook=cookbook, tag_from=tag_from, tag_to=tag_to), 
        headers={"Private-Token": "WD2aTjFrmHqyrTjGG-Ap"}).content)
    return responce

    

def cookbook_gitlab_prepare_link(cookbook=""):


    return "https://code.devops.fds.com/api/v4/projects/chef%2F{cookbook}/repository/tags".format(cookbook=cookbook)
    


if __name__ == "__main__":
    with open("supermarket.json") as supermarket_responce:
        supermarket_cookbooks = json.load(supermarket_responce)
        # print(supermarket_cookbooks)
    # Gitlab_requester = async_cookbooks_request()
    # for cookbook_data in supermarket_cookbooks.values():
    #     if cookbook_data["source_url"] != "None":
    #         Gitlab_requester.push_link(cookbook_data["source_url"])
            # print(cookbook_data["source_url"])
    # cookbooks_diffs = Gitlab_requester.async_call(custom_headers={"Private-Token": "WD2aTjFrmHqyrTjGG-Ap"})
    
    # responce = json.loads(
    #         requests.get("https://code.devops.fds.com/api/v4/projects", 
    #         headers={"Private-Token": "WD2aTjFrmHqyrTjGG-Ap"}).content)
    # # for entry in responce:
    #     for field in entry:
    #         if field == "id":
    #             print(entry["name"])
    tags = cookbook_get_tags("wssg")
    # print(commits_compare("wssg", "v4.44.0", "v4.45.0"))
    # for i in commits_compare("wssg", "v4.44.0", "v4.45.0")["commits"]:
    #     print(i["short_id"])
    # print(tags_and_dates)

    # print(take_commits_betw_two_dates("wssg", "20180913", "20181009"))
    cookbooks_diffs = compose_diff_for_every_tag("wssg", tags)
    # print(cookbooks_diffs)
    for tag, diff in cookbooks_diffs.items():
        print(tag)
        for info in diff:
            print(info["short_id"])

    

