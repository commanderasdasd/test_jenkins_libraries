#!/usr/bin/env groovy
import java.util.regex.*;
import groovy.json.*


def parse(fileContents) {
    def cookbooksWithVersions = [:]
    lines = fileContents.readLines()
    for(int i=0; i<lines.size(); i++) {
        line = lines[i]

        Matcher packageNameMatched = line =~ /\w{1,}(_|-)*\w*(_|-)*\w*/
        def packageName = packageNameMatched[0][0]
        Matcher versionsMatched = line =~ /\d{1,3}\.\d{1,3}\.\d{1,3}/
        def versions = []

        // versions.add("None")
        for(int j = 0; j < versionsMatched.size(); j++) {
            versionFound = versionsMatched[j]
            versions.add(versionFound)
        }
        cookbooksWithVersions[packageName] = versions   
    }
    return new JsonBuilder(cookbooksWithVersions).toPrettyString()
}

// @NonCPS
def prepareUI(cookbooksParsed) {

    // Iterating over Cookbooks parsed in groovy script
    // printf("${cookbooksSorted}")
    
    // sortVersions(cookbooksParsed)
    def inputAsMap = [
        disable_edit_json: false,
        disable_properties: true,
        no_additional_properties: true,
        disable_collapse: false,
        disable_array_add: true,
        disable_array_delete: true,
        disable_array_reorder: true,
        iconlib:"fontawesome4",
        theme: 'bootstrap2',
        schema:[:]
    ]

    inputAsMap.schema = [
        type: "object",
        format: "table",
        title: "Cookbooks",
        collapse: true,
        properties: [:]
    ]
    for ( entry in cookbooksParsed ) {
            // printf("${entry.value["versions"]}")
            printf("entry is here ${entry}")
            def cookbookName = entry.key
            def versions = ["None"] + entry.value["versions"]
            def cookbTempl = [
                    type: "array",
                    compact: true,
                    default: versions[0],
                    format: "single",
                    items: [
                        title: "${cookbookName}",
                        type: "string",
                        enum: versions
                    ],
                ]
            inputAsMap.schema.properties.put cookbookName, cookbTempl
        
    }

    inputAsMap
}

@NonCPS
def separateChanged(jsonData) {
    cookbooksNotChanged = []
    cookbooksChanged = [:]
    jsonData.each{ key, value -> 
        if (value[0] == "None") {
            cookbooksNotChanged += key
        } else {
            cookbooksChanged.put key, value
        }
    }
    return [cookbooksChanged, cookbooksNotChanged]
}

String padVersion(Integer ver){
    return String.format("%03d",ver)
}

// @NonCPS
def sortVersions(jsonData) {
    // jsonData.each{ cookbook, version_data ->
    for ( cookbook in jsonData ) {

        def cookbookContent = cookbook.value
        // print(cookbookContent)
        for ( entry in cookbookContent ) {
            // print(entry)
            def key1 = entry.key
            def value1 = entry.value
            // print(key1)
        // version_data.each { key1, value1 ->
            if (key1 == "versions" && value1 != null) {
                print(value1)
                sortingVersion(value1)
            // print("Sorted, ${}")
            value1.reverse()
            // jsonData[cookbook.key][entry.key] = value1.reverse()
            }
        }
    }
    printf("Sorted, ${jsonData}")
    return jsonData
}

@NonCPS
def sortingVersion(value) { 
    value.sort { a,b ->
                    padVersion(a.split('\\.').getAt(0).toInteger()) <=> padVersion(b.split('\\.').getAt(0).toInteger()) 
                    ?: padVersion(a.split('\\.').getAt(1).toInteger()) <=> padVersion(b.split('\\.').getAt(1).toInteger()) 
                    ?: padVersion(a.split('\\.').getAt(2).toInteger()) <=> padVersion(b.split('\\.').getAt(2).toInteger())
                }
    print(value)

}

// This has been used for debugging
def WORKSPACE = "/Users/atankovskii/git/test_libs/jenkins_home/workspace/Send-email-announcement"
String cookbooksJson = new File("${WORKSPACE}/UI.json").text
def jsonSlurper = new JsonSlurper()
def cookbooksParsed = jsonSlurper.parseText(cookbooksJson)

sortVersions(cookbooksParsed)
// print(cookbooksParsed)