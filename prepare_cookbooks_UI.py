"""
 Tags in repo marks cookbooks versions
"""

import os, sys, datetime, subprocess, requests, json, urllib.parse, pdb, grequests, logging, shutil, bittrex_websocket
#TODO: for debug
import random


""" logger setup """
LOGDIR = "./logs"
try:
    os.mkdir(LOGDIR)
except FileExistsError:
    print("log directory already exists")

Formatter = logging.Formatter('%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s')

logname = "{LOGDIR}/{date}_.log".format(date=datetime.datetime.now(), LOGDIR=LOGDIR)
logging.getLogger("urllib3").setLevel(logging.WARNING)
logging.basicConfig(level=logging.DEBUG,
                    filename=logname,
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S')
LOG = logging.getLogger(__name__)
print("\"{}\"".format(logname))

""" additional log stream """

handler_stdout = logging.StreamHandler(sys.stdout)
handler_stdout.setLevel(logging.DEBUG)
handler_stdout.setFormatter(Formatter)
LOG.addHandler(handler_stdout)


def berksfile_parse(berksfile_text):
    
    """Decorator parsing berksfile content with all cookbooks available for deploy"""


    berksfile_cookbooks = []
    for string in berksfile_text.splitlines():
        if string[0:8] == "cookbook":
            berksfile_cookbooks.append(string.split(" ")[1].replace('\'', "").rstrip(","))
    return berksfile_cookbooks

def berksfile_get():

    """return contents of berksfile"""

    return requests.get("https://code.devops.fds.com/api/v4/projects/devops%2Fchef/repository/files/Berksfile/raw?ref=master", headers={"Private-Token": "WD2aTjFrmHqyrTjGG-Ap"}).content.decode('utf-8')


class AsyncRequester():

    """stores links and request all in async way"""

    def __init__(self, custom_headers={}):
        self._links = []
        self._custom_headers = custom_headers

    def push_link(self, link):
        self._links.append(link)

    def exception(self, request, exception):
        LOG.error("Problem: {}: {}".format(request.url, exception))

    def async_call(self, ):
        results = grequests.map(
            (grequests.get(u, headers=self._custom_headers) for u in self._links),
            exception_handler=self.exception,
            size=10)
        return results


def supermarket_get_cookbooks(supermarket_url="https://supermarket.chef.io/"):
    """
    provide supermarket address with https://
    """

    api_url = urllib.parse.urljoin(supermarket_url, "api/v1/cookbooks")

    total_cookooks_count = int(json.loads(
                               requests.get(api_url).content.decode('utf-8')
                               )["total"])

    api_all_cookbooks_req = urllib.parse.urljoin(api_url,
                                             '?start=0&items={0}'.format(total_cookooks_count))

    all_cookbooks=json.loads(requests.get(api_all_cookbooks_req).content.decode('utf-8'))

    return all_cookbooks

def supermarket_parse_cookbooks(supermarket_responce, requester_instance, filter_list=[]):
    """
    in: 
        requester_instance for running requests to cookbook pages in parallel

    out: 
        cookbooks dictionary with versions and source urls
    """

    
    
    for item in supermarket_responce["items"]:
        cookbook_name = item.get("cookbook_name")
        cookbook_link = item.get("cookbook") 
        # print(item)

        if cookbook_name in filter_list:
            LOG.info("Cookbook {} present in berksfile".format(cookbook_name))
            requester_instance.push_link(cookbook_link)
    
    
    responces = requester_instance.async_call()

    cookbooks_parsed = {}

    for responce in responces:
        try:
            responce_content = json.loads(responce.content.decode('utf-8'))
            cookbook_name = responce_content["name"]
            cookbooks_parsed[cookbook_name] = {}

            if responce_content["source_url"] is None:
                source_url = responce_content.get("external_url")
            else:
                source_url = responce_content.get("source_url")
            
            cookbooks_parsed[cookbook_name]["source_url"] = source_url
            cookbooks_parsed[cookbook_name]["latest_version"] = responce_content.get("latest_version")

        except Exception as e:
            LOG.error("Error {} in {}". format(e, responce_content["name"]))

    return cookbooks_parsed


def cookbooks_get_organization_versions(server, filter_list=[]):

    """Collects current deployed version on server for every cookbook that present in filter_list"""

    knife_command = "knife cookbook list --server-url '{server}' " \
                    "--key '{workspace}/hudson/cloudpants/keys/chef.devops.fds.com_client.pem' " \
                    "--config '{workspace}/hudson/cloudpants/knife.rb'".format(server=server,
                                                                               workspace=os.environ['WORKSPACE'])

    try:
        output = subprocess.check_output(knife_command, shell=True, cwd=os.environ['WORKSPACE'])

    except subprocess.CalledProcessError as ex:
        LOG.error("Unable to get deployed cookbooks from {server}: {error}".format(server=server,
                                                                                   error=ex.output))
    cookbooks_deployed = {x.split()[0]: x.split()[1] for x in output.splitlines()}
    cookbooks_filtered = {}
    for cookbook, version in cookbooks_deployed.items():
        cookbooks_filtered[cookbook.decode("utf-8")] = version.decode("utf-8") 

    return cookbooks_filtered
    
def cookbook_get_tags(cookbook=""):

    """get all tags for cookbook from gitlab""" 
    
    responce = requests.get("https://code.devops.fds.com/api/v4/projects/chef%2F{cookbook}/repository/tags".format(cookbook=cookbook), 
                            headers={"Private-Token": "WD2aTjFrmHqyrTjGG-Ap"})
    tags = []
    if (responce.status_code == 200):
        responce_content = json.loads(responce.content.decode('utf-8'))
        for key in responce_content:
            # print("cookbook_get_tags's key {}".format(key))
            try:
                tags.append(key["name"])
            except Exception as e:
                LOG.debug("Error on key {}, in cookbook {}, error text: {}".format(key, cookbook, e))
                
    return tags

    
def compose_diff_for_every_tag(cookbook, tags):

    """Makes request for every pair of version tags and gets commits history between them"""
    
    tags_and_diffs = {}
    for counter, tag in enumerate(tags):
        if counter > 0:
            tag_cur = tags[counter]
            tag_prev = tags[counter-1]
            LOG.info("Comparing {} and {} for cookbook {}".format(tag_cur, tag_prev, cookbook))
            tags_and_diffs[tags[counter-1]] = compare_commits(cookbook, tag_cur, tag_prev)
    LOG.debug("Returning from compose_diff_for_every_tag {}".format(tags_and_diffs))

    return tags_and_diffs


def compare_commits(cookbook, tag_from, tag_to):

    """ requsts to gilab API for commits history between pair of version tags """

    responce = json.loads(
        requests.get("https://code.devops.fds.com/api/v4/projects/chef%2F{cookbook}/repository/compare?from={tag_from}&to={tag_to}".format(cookbook=cookbook, tag_from=tag_from, tag_to=tag_to), 
        headers={"Private-Token": "WD2aTjFrmHqyrTjGG-Ap"}).content.decode('utf-8'))
    
    LOG.debug("Compare commits returning {}".format(responce))

    return responce


def versions_sort_key(version):

    """used as key= parameter for sorted"""

    LOG.debug("version in versions_sort_key {}".format(version))
    version_tag_split = version.split(".")

    return version_tag_split[0], version_tag_split[1], version_tag_split[2]


def filter_tags(tag_organisation, tags_all):

    """Filters cookbooks versions lower than tag_organization (already deployed cookbook)"""

    # tag_organisation = tag_organisation.decode("utf-8") 
    tags_filtered = []
    LOG.debug("Comparing version current deployed: {} \n with all: {}".format(tag_organisation, tags_all))
    index_lowest = 0
    if tag_organisation is None:
        tags_filtered = tags_all
    else:
        if (tags_all):
            sorted(tags_all, key=versions_sort_key)
            for index, version in enumerate(tags_all):
                version = version.replace("v", "")
                if (tag_organisation == version):
                    index_lowest = index
                    LOG.debug("Common version, index of deployed version is {}".format(index_lowest))
                else:
                    LOG.debug("Not found matching tag in version history for {}".format(tag_organisation))
            tags_filtered = tags_all[0:index_lowest]
        else:
            LOG.debug("Not tag list is empty, nothing to compare with {}".format(tag_organisation))

    return tags_filtered

    




def prepare_json_for_input_step(cookbooks_diff, current_versions):

    """forming output UI.json which will be used in interface script"""

    json_form = {}
    LOG.debug("All data comes in bulder: {}, current version is {}".format(cookbooks_diff, current_versions))

    for cookbook, data_cookbook in cookbooks_diff.items():
        json_form[cookbook] = {}
        json_form[cookbook]["versions"] = []
        json_form[cookbook]["description"] = {}
        try:
            json_form[cookbook]["description"]["current"] = current_versions[cookbook]
        except KeyError:
            LOG.error("No current version for {}, setting None".format(cookbook))
            json_form[cookbook]["description"]["current"] = "None"
        json_form[cookbook]["description"]["versions"] = {}
        if (data_cookbook):
            for version, data_version in data_cookbook.items():
                version = version.replace("v", "")
                json_form[cookbook]["versions"].append(version)
                json_form[cookbook]["description"]["versions"][version] = {}
                json_form[cookbook]["description"]["versions"][version]["ticket"] = "TODO Jira ticket link"
                json_form[cookbook]["description"]["versions"][version]["description"] = "TODO version description"
                json_form[cookbook]["description"]["versions"][version]["commits"] = {}
                LOG.debug("Message is: {}".format(data_version["commits"]))
                for data_commit in data_version["commits"]:
                    json_form[cookbook]["description"]["versions"][version]["commits"][data_commit["short_id"]] = {}
                    message = data_commit["message"].replace("\n", "").replace("\"", "").replace("\'","").replace("\r","")
                    # LOG.debug(message)
                    json_form[cookbook]["description"]["versions"][version]["commits"][data_commit["short_id"]]["message"] = message
                    json_form[cookbook]["description"]["versions"][version]["commits"][data_commit["short_id"]]["author"] = data_commit["committer_name"].replace("\\", "")
                    json_form[cookbook]["description"]["versions"][version]["commits"][data_commit["short_id"]]["mailto"] = data_commit["committer_email"]
        else:
            LOG.info("Cookbook {} already latest version".format(cookbook))
            try:
                version = current_versions[cookbook]
                json_form[cookbook]["description"]["versions"][version] = {}
                json_form[cookbook]["description"]["versions"][version]["ticket"] = ""
                json_form[cookbook]["description"]["versions"][version]["description"] = "Already newest version"
                json_form[cookbook]["description"]["versions"][version]["commits"] = {}
                
            except KeyError:
                LOG.error("No current version for {}, setting None".format(cookbook))
                json_form[cookbook]["description"]["current"] = "None"

    return json_form
    



def main():
    """ 
    Takes berksfile 
    """
    UI_file_path = os.path.join(os.environ['WORKSPACE'], "UI.json")

    berksfile_content = berksfile_get()
    
    berksfile_cookbooks = berksfile_parse(berksfile_content)

    # TODO: debug only, remove in production
    # for counter, cookbook in enumerate(berksfile_cookbooks):
    #     if cookbook == "loyalty_ui":
    #         berksfile_cookbooks = berksfile_cookbooks[counter]

    latest_versions = cookbooks_get_organization_versions(server=os.environ['SERVER'])
    
    LOG.debug("Versions in orgnizations {}".format(latest_versions))
    
    supermarket_responce = supermarket_get_cookbooks(supermarket_url="https://chef-supermarket.devops.fds.com")

    Supermarket_requester = AsyncRequester()

    cookbooks_parsed = supermarket_parse_cookbooks(supermarket_responce, 
                                                Supermarket_requester, 
                                                filter_list=berksfile_cookbooks)

    LOG.debug("cookbooks_parsed is {}".format(cookbooks_parsed))
    cookbooks_diffs = {}

    for cookbook in cookbooks_parsed:
        tags_and_dates = cookbook_get_tags(cookbook)
        tags_filtered = filter_tags(latest_versions.get(cookbook), tags_and_dates)
        cookbooks_diffs[cookbook] = compose_diff_for_every_tag(cookbook, tags_filtered)
        LOG.debug("cookbooks diff structure at main function {}".format(cookbooks_diffs[cookbook]))
    

    UI_json = prepare_json_for_input_step(cookbooks_diffs, latest_versions)

    if os.path.exists(UI_file_path):
        os.remove(UI_file_path)
    with open(UI_file_path, 'w') as UI_file:
        json.dump(UI_json, UI_file, separators=(',',':'), ensure_ascii=True)

if __name__ == "__main__":
    main()
    